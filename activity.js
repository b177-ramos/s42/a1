/*Activity Instructions:
1. Copy the index.html and index.js to the a1 folder.

2. Listen to an event when the last name's input is changed.

3. Instead of anonymous functions for each of the event listener:
  - Create a function that will update the span's contents based on the value of the first and last name input fields.
  - Instruct the event listeners to use the created function.*/

let firstNameLabel = document.querySelector("#txt-first-name")
let lastNameLabel = document.querySelector("#txt-last-name")
let fullNameDisplay = document.querySelector("#full-name-display")
let submit = document.querySelector("#submit-btn")

const showName = () => {
	fullNameDisplay.innerHTML = `${firstNameLabel.value} ${lastNameLabel.value}`
}

const alertValidation = () => {
	if(firstNameLabel.value && lastNameLabel.value){
		alert(`Thank you for registering ${firstNameLabel.value} ${lastNameLabel.value}`)
		firstNameLabel.value = ""
		lastNameLabel.value = ""
		fullNameDisplay.innerHTML = ""
	}
	else{
		alert("Please input firsname and lastname.")
	}
}

firstNameLabel.addEventListener('keyup',showName)
lastNameLabel.addEventListener('keyup',showName)

submit.addEventListener('click',alertValidation)